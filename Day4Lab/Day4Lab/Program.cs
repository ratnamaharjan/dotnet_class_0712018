﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4Lab
{
    class Program
    {
        static void Main(string[] args)
        {
            //Car honda = new Car();
            //honda.Type = "Sedan";
            //honda.Cost = 5000; 



            //Car toyota = new Car("sedan");
            //Car bmw = new Car("coupe", 85000.00);



            //Employee emp = new Employee();           
            //emp.EmployeeId = 9999;
            //emp.Address = "Colorado";
            //Console.WriteLine(emp.EmployeeId + " " + emp.Address);

            //Employee employee = new Employee(2000, "Irving, TX");
            //Console.WriteLine(employee.EmployeeId + " " + employee.Address);

            //Employee emp2 = new Employee();


            //public
            //EmployeeDetails employeeDetails = new EmployeeDetails();
            //employeeDetails.Address = "A   ";
            //employeeDetails.Salary = 50000;
            //employeeDetails.GetDetails();

            //Department d = new Department();

            Car c = new Car();
            var model = c.GetCarModel("Sedan");

            List<Car> carList = new List<Car>();
            Car honda1 = new Car();
            Car honda2 = new Car();
            Car honda3 = new Car();
            Car honda4 = new Car();

            carList.Add(honda1);
            carList.Add(honda2);

            Console.WriteLine(model);
            Console.ReadKey();
        }
    }
}
