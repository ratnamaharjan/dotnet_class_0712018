﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsExample
{
    class Program
    {
        static void Main(string[] args)
        {
            MyGeneric<string> myVariable = new MyGeneric<string>("test");
            myVariable.PrintDetail();

            MyGeneric<double> param = new MyGeneric<double>(700.00);
            myVariable.PrintDetail();

            Employee e = new Employee();
            Employee e2 = new Employee();
            MyGeneric<Employee> myEmployee = new MyGeneric<Employee>(e);
            myEmployee.PrintDetail();


            var cmpObject = new CommonComparision("test");
            cmpObject.Compare<Employee>(e, e2);

            cmpObject.Compare<string>("First", "Last");



            GenericClassWithConstraint<Employee> a = new GenericClassWithConstraint<Employee>();
            GenericClassWithConstraint<string> b = new GenericClassWithConstraint<string>();

            GenericClassWithNew<Employee> ee = new GenericClassWithNew<Employee>();

            GenClassWithImpl<Employee> empTest = new GenClassWithImpl<Employee>();

            GenTisSomething<Employee> somethingExample = new GenTisSomething<Employee>(); 

            cmpObject.Compare<Employee>(e, e2);

            Console.ReadKey();
        }
    }

    public class GenClassWithImpl<T> where T : IEmpService
    {
    }

    public interface IEmpService
    {
        void PrintEmployee();
    }
}
