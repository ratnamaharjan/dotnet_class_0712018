﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CerotidFirstWeb
{
    public partial class _Default : Page
    {
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["mydemoviewstate"] = "test";
            if (!IsPostBack)
            {
                btnTest.Text = "Click Me";
                txtFirstName.Text = "Change Me";
            }            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var viewState = ViewState;
        }

        protected void txtFirstName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}