﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsExample
{
    public class MyGeneric<T>
    {
        private T myClassVariable;

        public MyGeneric(T someValue)
        {
            myClassVariable = someValue;
        }
        
        public void PrintDetail()
        {
            Console.WriteLine("Printing Type of T {0}", myClassVariable.GetType());
        }        
    }

    public class GenericClassWithConstraint<T> where T : class
    {

    }

    public class GenericClassWithNew<R> where R : new()
    {

    }

    public class GenTisSomething<T> where T : Employee
    {

    }

    public class CommonComparision
    {
        public CommonComparision(string compareString)
        {
        }
        public void Compare<T>(T par1, T par2) where T : class
        {

        }
    }
}
