﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CerotidFirstWeb._Default" EnableViewState="true" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                <input type="button" value="Input Button" id="btnInputButton" />
                <asp:Button ID="btnTest" runat="server" Text="My First Button" OnClick="Button1_Click" />
                <br />
                <asp:TextBox ID="txtFirstName" runat="server" OnTextChanged="txtFirstName_TextChanged" CssClass="someclass"></asp:TextBox>
                </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>      
       
    </div>

</asp:Content>
