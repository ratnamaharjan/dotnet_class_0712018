﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTypeLesson
{
    class Program
    {
        static void Main(string[] args)
        {
            //<data type> variableName = (assignment) 10 (value) ; 
            //int myNumber = 10;
            //short shortNumber = 55;
            //const double PI = 3.14159;
            //const string API_BASEURL = "http://www.google.com/api/"; 
            //char flag = 'T';

            //int firstNumber = 20;

            //int secondNumber = firstNumber++;
            //int sum = firstNumber + secondNumber;
            //Console.WriteLine(sum);

            //////string 
            ////string firstName = "John";
            ////string lastName = "smith";

            //Employee e = new Employee();
            //e.FirstName = "John";
            //e.LastName = "Smith";


            //Console.WriteLine("your employee info :" + e.FirstName + " " + e.LastName);

            //Type Casting 
            //string to int 
            //Console.WriteLine("Enter your age");
            //int age = int.Parse(Console.ReadLine());

            //int futureAge = 10 + age;
            //Console.WriteLine("Future age " + futureAge); 

            //short a = 10;
            //short b = 20;
            //short retsult = (short)(a + b);
            //Console.WriteLine(retsult);

            //int? externalData = 200 ;
            //double? someDouble = null; 
            //DateTime? date = null;
            //DateTime myDate = DateTime.Now;

            //double p = someDouble ?? 0.0 + 10.55; 

            //int myData = externalData ?? 10; 
            //Console.WriteLine("my value of i = " + myData);

            ////boxing --> convert value type to reference type. 
            //int index = 10;
            //object obj = index;

            ////unboxing --> convert reference type to value type. 
            //int resultValue = (int)obj;

            int firstNumber = 10;
            int second = 30;

            //if (firstNumber == second)
            //    Console.WriteLine("Conditon Check using ==" );
            //if (firstNumber != second)
            //    Console.WriteLine("Conditon Check with !=");
            //if (firstNumber.Equals(second))
            //    Console.WriteLine("Conditon Check using .equal");

            //if (firstNumber <= 10 && second < 50)
            //{
            //    Console.WriteLine("I am true");
            //}
            //else
            //{
            //    Console.WriteLine("I am false");
            //}

            //int p = 5;
            //p++; // p = p+1
            //p--; // p = p-1
            //int q = 10;
            //p += q; //p = p+q;
            int q = 15;
            // true ? <this value> : <that value>
            int newNumber = q == 15 ? 20 : 40;

            //if (q == 15)
            //{
            //    newNumber = 20;
            //}
            //else
            //{
            //    newNumber = 40;
            //}

            //Console.WriteLine("new number = " + newNumber);



            /*
             * Flow Controls
if statement
if else
else
switch
while 
for
foreach
continue

             */
            // if(expression evalues to true)
            string day = Console.ReadLine();
            //string finalValue = gender == "male" ? "It is Male" : "It is not Male";

            //Console.WriteLine(finalValue);


            //if (gender == "male")
            //{
            //    Console.WriteLine("Evaluated to true (if)");
            //}
            //else if (gender == "female")
            //{
            //    Console.WriteLine("Evaluated else if");
            //}
            //else
            //{
            //    Console.WriteLine("Evaluated to else");
            //}
            string dayType = null;
            switch (day)
            {
                case "Sunday":
                case "Saturday":
                    //do something
                    {
                        dayType = "weekend";                        
                    }
                    break;
                case "Wednesday":
                    //do something else
                    dayType = "Humpday";
                    break;
                default:
                    //do default thing
                    dayType = "weekday";
                    break;
            }
            Console.WriteLine(dayType);
            Console.ReadKey();

        }
    }

    class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
