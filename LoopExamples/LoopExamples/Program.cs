﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoopExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            //while loop
            //int startNumber = 11;
            //Console.WriteLine("while ");
            //while (startNumber <= 10)
            //{
            //    Console.WriteLine("Number = " + startNumber);
            //    startNumber++; //startNumber = startNumber + 1;
            //}

            //Console.WriteLine("do - while");
            //startNumber = 0;
            //do
            //{
            //    Console.WriteLine("Number = " + startNumber);
            //    startNumber++;
            //}
            //while (startNumber <= 10); //condition

            //for (int i = 0; i < 10; i++)
            //{
            //    if (i == 4)
            //    {
            //        continue;
            //    }
            //    Console.WriteLine("for loop Number = " + i);
            //    if (i == 7)
            //    {
            //        break;
            //    }
            //    //startNumber = startNumber + 1;
            //}

            //Gender male = Gender.Male;
            //Gender female = Gender.Female;

            //Console.WriteLine("Value of Male " + (int)male);
            //Console.WriteLine("Value of Female " + (int)female);

            //var gasPriceData = new GasPrice { Unleaded = 2.29, Suprem = 5.99 };

            //Console.WriteLine("Gas Price: \nUnleaded {0} \nSuprem {1}", gasPriceData.Unleaded, gasPriceData.Suprem);

            //int[] myArray = new int[10];
            //string[] myStringArray = new string[10]; 


            //for (int i = 0; i < 10; i++)
            //{
            //    myArray[i] = i * 4; 
            //}

            //for (int j = 0; j < myArray.Length; j++)
            //{
            //    Console.WriteLine("Index {0} Value on Array {1}", j, myArray[j]);
            //}

            //Employee[] myEmployees = new Employee[10];
            //for (int i = 0; i < 10; i++)
            //{
            //    //object of employee === 1 instance
            //    Employee emp = new Employee();
            //    emp.FirstName = "FirsName" + i;
            //    emp.Salary = 500 * (i + 1);

            //    myEmployees[i] = emp;

            //}

            //for (int j = 0; j < myEmployees.Length; j++)
            //{
            //    Console.WriteLine("FirsName : {0} Salary {1}", myEmployees[j].FirstName, myEmployees[j].Salary);
            //}

            double[,] myMultiDimArray = new double[2, 2];
            myMultiDimArray[0, 0] = 1;
            myMultiDimArray[0, 1] = 2;
            myMultiDimArray[1, 0] = 3;
            myMultiDimArray[1, 1] = 4;

            Console.WriteLine("multi array vaues {0} , {1}", myMultiDimArray[0, 0], myMultiDimArray[1, 1]); 
            Console.ReadKey();
        }
    }
}
