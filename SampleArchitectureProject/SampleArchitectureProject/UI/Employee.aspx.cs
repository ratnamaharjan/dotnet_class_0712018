﻿using SampleArchitectureProject.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SampleArchitectureProject.UI
{
    public partial class Employee : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            EmployeeService employeeService = new EmployeeService();
            var data = employeeService.GetEmployeeData();
            GridView1.DataSource = data;
            GridView1.DataBind();
        }

    }
}