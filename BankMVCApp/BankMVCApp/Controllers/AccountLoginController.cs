﻿using BankDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BankMVCApp.Controllers
{
    public class AccountLoginController : Controller
    {
        [HttpGet]
        // GET: AccountLogin
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(UserLogin model)
        {
            bool isValidated = false;
            string errorMessage = string.Empty;
            try
            {
                //Authenticate against our DB
                BankDBEntities dBEntities = new BankDBEntities();
                var data = dBEntities.UserLogins
                    .Where(x => x.UserName == model.UserName && x.Password == model.Password).FirstOrDefault();
                if (data != null)
                {
                    isValidated = true;
                }
                if (isValidated)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, true);
                    return RedirectToAction("Index", "Bank");
                }
                else
                {
                    errorMessage = "Unable to validate. Authentication failed!";
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            ViewBag.AuthMessage = errorMessage;
            return View("Index");
        }

        public ActionResult LogOff()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return View("Index");
        }
    }
}