﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4Lab
{
    public class Car
    {
        public string Type { get; set; }
        public double Cost { get; set; }

        public int Mileage {
            get
            {
                if (Cost <= 5000)
                    return 7000;
                else
                    return 10000;
            }
            set
            {
                //logic
            }
        }

        public Car()
        {
            Console.WriteLine("Car with default constructor is called");
        }

        public Car(string type)
        {
            Type = type;
            Console.WriteLine("Car with 1 parameter constructor is called");
        }

        public Car(string type, double cost)
        {
            Type = type;
            Cost = cost;
            Console.WriteLine("Car with 2 parameters constructor is called");
        }

        public string GetCarModel(string type)
        {
            //

            return "Model M"; //""
        }

    }
}
