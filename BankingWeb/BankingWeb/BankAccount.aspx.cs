﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bank.Model;
using System.Data.SqlClient;
using System.Data;

namespace BankingWeb
{
    public partial class BankAccount : System.Web.UI.Page
    {
        List<Account> accountList = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var msg = Request.QueryString["msg"];
                if (msg != null)
                {
                    lblErrorMessage.Text = msg.ToString();
                }
                var dataSet = GetAccounts();
                gvAccounts.DataSource = dataSet;
                gvAccounts.DataBind();
            }
        }

        protected void btnAddAccount_Click(object sender, EventArgs e)
        {
            //create account object
            Account account = new Account();
            account.AccountNumber = txtAccountNumber.Text;
            account.AccountType =  (AccountType)Enum.Parse(typeof(AccountType), ddlAccountType.SelectedItem.Value);
            account.CurrentBalance = Convert.ToDouble(txtCurrentBalance.Text);
            account.AccountOpenDate = DateTime.Now;

            //var countrySelect = RadioButtonList1.SelectedValue;
            //var sampleCheckBoxSelected = CheckBoxList1.SelectedValue;

            var rowsaffected = InsertAccount(account);

            var dataSet = GetAccounts();

            gvAccounts.DataSource = dataSet;
            gvAccounts.DataBind();
            //reset inputs
            ResetControlsAfterAdd();
        }

        private void ResetControlsAfterAdd()
        {
            txtAccountNumber.Text = string.Empty;
            txtCurrentBalance.Text = string.Empty;
            ddlAccountType.SelectedIndex = 0;
        }

        private int InsertAccount(Account account)
        {
            try
            {
                //getting connection string from web config
                string conn = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ToString();
                //creating sql connection object
                using (SqlConnection sqlConn = new SqlConnection(conn))
                {
                    //Open Database Connection 
                    sqlConn.Open();
                    //SqlCommand cmd = new SqlCommand("INSERT INTO Account(AccountType, AccountNumber, AvailableBalance) VALUES('"
                    //                    + account.AccountType + "','" + account.AccountNumber + "'," + account.CurrentBalance + ")"
                    //                    , sqlConn);
                    SqlCommand cmd = new SqlCommand("usp_InsertAccount", sqlConn);
                    cmd.Parameters.AddWithValue("@accountNumber", account.AccountNumber);
                    cmd.Parameters.AddWithValue("@accountType", account.AccountType);
                    cmd.Parameters.AddWithValue("@balance", account.CurrentBalance);
                    var result = cmd.ExecuteNonQuery();
                    sqlConn.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }
            return 0;
        }

        private DataSet GetAccounts()
        {
            try
            {
                //getting connection string from web config
                string conn = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ToString();
                //creating sql connection object
                using (SqlConnection sqlConn = new SqlConnection(conn))
                {
                    sqlConn.Open();
                    //sql command
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = sqlConn;
                    //cmd.CommandText = "SELECT AccountType,AccountNumber,[AvailableBalance] AS CurrentBalance,[OpenedDate] AccountOpenDate,[ClosedDate] FROM [AccountsDB].[dbo].[Account]";
                    //cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "[usp_GetAccountData]";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //adapter
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    //dataset object
                    DataSet ds = new DataSet();
                    //fill dataset 
                    da.Fill(ds);
                    sqlConn.Close();
                    return ds;
                }
                
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }
            return null;
        }

        protected void gvAccounts_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Get Selected Row
            GridViewRow grow = gvAccounts.SelectedRow;

            //Get selected Row item id
            var accountNumber = grow.Cells[2].Text;

            var redirectUrl = "AccountDetail.aspx?id=" + accountNumber;
            Response.Redirect(redirectUrl);
        }
    }
}