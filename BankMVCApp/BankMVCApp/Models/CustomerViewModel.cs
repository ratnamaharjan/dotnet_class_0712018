﻿using BankDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankMVCApp.Models
{
    public class CustomerViewModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public Address MailingAddress { get; set; }
        public Address BillingAddress { get; set; }
    }
}