﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class Account
    {
        /*In Account - AccountType: String
       Behavior- PrintAccountInfo
         */
        //public string AccountType { get; set; }
        public AccountType AccountType { get; set; }
        public string AccountNumber { get; set; }
        public double CurrentBalance { get; set; }

        public DateTime AccountOpenDate { get; set; }
        public DateTime AccountCloseDate { get; set; }


        public void PrintAccountInfo()
        {
            Console.WriteLine("** start PrintAccountInfo**");
            Console.WriteLine("Account Type: {0}, Account Number {1}, CurrentBalance: {2}", AccountType, AccountNumber, CurrentBalance.ToString("c"));
            Console.WriteLine("** end PrintAccountInfo**");
        }

        public void SendMoney()
        {
            //provide option to chose account : AccountTypes
            string accountOption = string.Empty;
            
            while (true)
            {
                Console.WriteLine($"Choose Account to Send Money:\n1. " +
                $"{AccountType.Saving} \n2. {AccountType.Checking} " +
                $"\n3. {AccountType.BusinessChecking} ");
                var selectedOption = Console.ReadLine();
                bool validSelection = false;
                switch (selectedOption)
                {
                    case "1":
                        accountOption = AccountType.Saving.ToString();
                        validSelection = true;
                        break;                        
                    case "2":
                        accountOption = AccountType.Checking.ToString();
                        validSelection = true;
                        break;
                    case "3":
                        accountOption = AccountType.BusinessChecking.ToString();
                        validSelection = true;
                        break;
                    default:
                        Console.WriteLine("Invalid option! Please Select correct option");
                        break;
                }
                if (validSelection)
                {
                    Console.WriteLine($"You have selected {accountOption} account!");
                    break;
                }
            }
            //provide option to choose transaction : WireTransfer, EC, MG

        }

    }
}
