﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Bank.Model
{
    public class FileHelper
    {
        public static List<string> ReadCustomerDataFromFile(string filePath)
        {
            try
            {
                if (!File.Exists(filePath))
                {
                    Console.WriteLine("File {0} does not exists!", filePath);
                }
                else
                {
                    var result = File.ReadAllLines(filePath);
                    return result.ToList();
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception while reading file. Message {0}", ex.Message);
            }
            return null;
        }

        public static void WriteToOutputFile(List<Customer> customers, string outPutFilePath)
        {
            try
            {

                if (File.Exists(outPutFilePath))
                {
                    File.Delete(outPutFilePath); 
                }
                
                foreach (Customer cust in customers)
                {
                    File.AppendAllText(outPutFilePath, cust.LastName + "," + cust.FirstName + Environment.NewLine);                  
                }                    
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while writing to the file {0}", ex.Message);
            }
        }
    }
}
