﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class MoneyGram : Transaction 
    {
        public string DestinationCountry { get; set; }
        public DeleveryOptions DeleveryOption { get; set; }
    }

    public enum DeleveryOptions
    {
        TenMinute,
        TwentyFourHrs
    }
}
