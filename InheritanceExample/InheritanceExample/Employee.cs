﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExample
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual void GetData()
        {
            Console.WriteLine("Employee - Getdata");
        }
    }

    public class Manager : Employee
    {
        
    }

    public class PartTimeEmplpoyee : Employee
    {
    }
   
}
