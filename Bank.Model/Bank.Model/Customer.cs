﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class Customer
    {
        public string CustomerName
        {
            get { return FirstName + " " + LastName; }
            set { }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<Account> Accounts { get; set; }

        public string Address { get; set; }
        
        public Customer()
        {
            Accounts = new List<Account>();
        }

        public Customer(string custName, string add)
        {
            CustomerName = custName;
            Address = add;
            Accounts = new List<Account>();
        }

        public void PrintCustomerAccounts()
        {
            Console.WriteLine("** start PrintCustomerAccounts**");
            foreach (Account act in Accounts)
            {
                Console.WriteLine("Account Type: {0}, Account Number: {1}", act.AccountType, act.AccountNumber);
            }
            Console.WriteLine("** Finished PrintCustomerAccounts**");

        }

        public double GetActTotal()
        {
            return GetAccountTotal();
        }

        private double GetAccountTotal()
        {
            double accountTotal = 0;
            foreach (Account act in Accounts)
            {
                accountTotal = accountTotal + act.CurrentBalance;
            }
            return accountTotal;
            //return Accounts.Sum(x => x.CurrentBalance);            
        }

        public void PrintCustomerDetails()
        {
            Console.WriteLine("** start PrintCustomerDetails**");
            Console.WriteLine("Customer Name: {0}, Address : {1}", CustomerName, Address);
            Console.WriteLine("** End PrintCustomerDetails**");
        }


    }
}
