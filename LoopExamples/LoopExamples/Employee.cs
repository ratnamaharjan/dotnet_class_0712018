﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoopExamples
{
    public class Employee
    {
        public string FirstName { get; set; }
        public double Salary { get; set; }
    }
}
