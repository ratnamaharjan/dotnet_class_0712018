﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoopExamples
{
    public enum Gender
    {
        Male = 15,
        Female = 20
    }

    public struct GasPrice
    {
        public double Unleaded { get; set; }
        public double Suprem { get; set; }
    }

}
