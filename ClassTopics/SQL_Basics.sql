-- Create Database
CREATE DATABASE BankDB
GO
-- Tables 
CREATE TABLE AccountType
(
	AccountTypeId INT PRIMARY KEY IDENTITY(1,1),
	AccountType NVARCHAR(50)
)
GO

CREATE TABLE Account
(
	AccountId INT PRIMARY KEY IDENTITY(1,1),
	AccountNumber NVARCHAR(75) NOT NULL,
	AvailableBalance DECIMAL(18,2) NOT NULL,
	OpenedDateTime DateTime,
	ClosedDateTime DATETIME,
	AccountTypeId INT REFERENCES AccountType (AccountTypeId)
)
GO

-- InSERT data
Insert INTO AccountType (AccountType) VALUES ('Checking'),
											 ('Saving'),
											  ('Business_Checking')
-- Select command
SELECT * from AccountType

INSERT INTO Account VALUES ('559898', 5000, GETDATE(), null, 1)
INSERT INTO Account VALUES ('559899', 3000, GETDATE(), null, 2)

-- Update command 
Update Account SET AccountNumber = '559888', AvailableBalance = 4000 
WHERE AccountId = 3
SELECT * from Account

-- Delete command 
DELETE FROM Account WHERE AccountId = 4
SELECT * from Account

--INNER JOIN / JOIN -- common data between two tables 

--RIGHT JOIN / RIGHT OUTER JOIN

 
 SELECT AccountId, AccountNumber, AvailableBalance, OpenedDateTime, ClosedDateTime, AccountType
 FROM Account JOIN AccountType 
	ON Account.AccountTypeId = AccountType.AccountTypeId

--LEFT JOIN / LEFT OUTER JOIN -- all data from left table and common records from right table
SELECT * 
FROM AccountType as at LEFT JOIN Account as a
	ON at.AccountTypeId = a.AccountTypeId

--FULL OUTER JOIN / FULL JOIN
SELECT * 
FROM AccountType as at FULL JOIN Account as a
	ON at.AccountTypeId = a.AccountTypeId

Update Account SET Account.ClosedDateTime = GETDATE()
FROM Account JOIN AccountType 
	ON Account.AccountTypeId = AccountType.AccountTypeId
WHERE AccountType.AccountType = 'Checking'
GO

-- Stored Procedure 
CREATE PROC usp_GetAccountData
AS
BEGIN 
	 SELECT AccountId, AccountNumber, AvailableBalance, OpenedDateTime, ClosedDateTime, AccountType
	FROM Account JOIN AccountType 
	ON Account.AccountTypeId = AccountType.AccountTypeId
END
GO

CREATE PROC usp_GetAccountDataByAccountId
(
	@accountId INT
)
AS
BEGIN 
	SELECT AccountId, AccountNumber, AvailableBalance, OpenedDateTime, ClosedDateTime, AccountType
	FROM Account JOIN AccountType 
	ON Account.AccountTypeId = AccountType.AccountTypeId
	Where AccountId = @accountId
END

EXEC usp_GetAccountData 
EXEC [dbo].[usp_GetAccountDataByAccountId] 2
GO
-- CREATE SCALAR function 
CREATE Function fn_GetAccountBalance(@accountId INT)
RETURNS DECIMAL(18,2)
AS
BEGIN
	RETURN (SELECT AvailableBalance
	FROM Account WHERE AccountId = @accountId)
END 

SELECT dbo.fn_GetAccountBalance(2)