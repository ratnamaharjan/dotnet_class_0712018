﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Hello " + args[0]);
        //    Console.ReadLine();
        //}

        static void Main(string[] args)
        {
            Console.WriteLine("Name : ");
            string name = Console.ReadLine();
            Console.WriteLine("Age :");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Hello " + name + " " + age);
            Console.WriteLine($"Hello {name}! Your age is {age}");
            Console.ReadLine();
        }
    }
}
