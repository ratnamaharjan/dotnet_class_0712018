﻿using System;
using MyClassLib;

namespace FirstConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * <print> Enter Your Name: 
	            <user input> ------
	            <print> Hi There -----! 
	            <print> Where are you From?: 
	            <user input> -------
	            <print> Oh you are from ----. I am also from -----
	            <print> Great!! 
             */
            Console.WriteLine("Enter Your Name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Hi There " + name + "!");

            Console.ReadKey();
        }
    }
}
