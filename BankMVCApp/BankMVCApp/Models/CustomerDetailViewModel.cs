﻿using BankDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankMVCApp.Models
{
    public class CustomerDetailViewModel
    {
        //Customer info
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? CustomerSince { get; set; }
        //Address Details
        public List<Address> Addresses { get; set; }
        //Acctont Details
        public List<Account> Accounts { get; set; }
    }
}