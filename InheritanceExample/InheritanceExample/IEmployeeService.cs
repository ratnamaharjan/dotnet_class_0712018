﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExample
{
    public interface IEmployeeService
    {
        void AddEmployee(Employee emp);
        bool UpdateEmployee(Employee emp);
        List<Employee> GetEmployees();
    }

    public abstract class Customer
    {
        public abstract void DoWork();

        public void HelloTest()
        {

        }
    }

    public abstract class CustData
    {
    }

    public class CustTest 
    {
    }

    public interface IAccountService
    {
        void AddAccount();
    }

    public class EmployeeService : CustData, IEmployeeService, IAccountService
    {
        public List<Employee> GetEmployees()
        {
            return null;
        }

        public bool UpdateEmployee(Employee emp)
        {

            return true;
        }

        public void AddEmployee(Employee emp)
        {
            Console.WriteLine(emp.FirstName + " " + emp.LastName);
        }

        public void AddAccount()
        {

        }
    }
}
