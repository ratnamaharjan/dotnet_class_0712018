﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Model;

namespace BankTester
{
    class Program
    {
        static void Main(string[] args)
        {
            Account act1 = new Account { AccountNumber = "883993838", AccountType = AccountType.Saving, CurrentBalance = 55000 };
            act1.SendMoney();
            Console.ReadKey();
        }

        private static void TestPreviousClass()
        {
            Bank.Model.Bank bank = new Bank.Model.Bank();
            bank.BankName = "Bank of America, Dallas";
            //bank.Customers
            //bank.PrintBankName();


            Customer customer1 = new Customer();
            customer1.CustomerName = "John Smith";
            customer1.Address = "330 BeltLine Rd";
            Account act1 = new Account { AccountNumber = "883993838", AccountType = AccountType.Saving, CurrentBalance = 55000 };
            act1.PrintAccountInfo();
            Account act2 = new Account { AccountNumber = "7887777", AccountType = AccountType.Checking, CurrentBalance = 44000 };
            //act2.PrintAccountInfo();
            customer1.Accounts = new List<Account>();
            customer1.Accounts.Add(act1);
            customer1.Accounts.Add(act2);
            Console.WriteLine("Account Total: {0}", customer1.GetActTotal());
            //customer1.PrintCustomerAccounts();
            //customer1.PrintCustomerDetails();
            bank.Customers.Add(customer1);

            //Customer customer2 = new Customer();
            //customer2.CustomerName = "Jim Smith";
            //customer2.Address = "330 Irving Rd";
            Customer customer2 = new Customer("Jim Smith", "330 Irving Rd");
            customer2.PrintCustomerAccounts();
            bank.Customers.Add(customer2);

            Customer customer3 = new Customer();
            customer3.CustomerName = "Mary Smith";
            customer3.Address = "330 Beltway Rd";
            bank.Customers.Add(customer3);

            //bank.PrintBankDetails();
        }
    }
}
