1. Create project- Bank.Model --> .Net Library type
2. Create class under above project - Bank, Customer, Account
3. In Bank - Properties - BankName: String; Customers: List<Customer>
        Behavior - PrintBankName
                   PrintBankDetails (BankName with associated Customers)
4. In Customer - CustomerName: String; Accounts : List <Account>; Address: String
	Behavior - printCustomerAccounts
	           printCustomerDetails (Example: Name, Accounts, Address)
5. In Account - AccountType: String
       Behavior- PrintAccountInfo
6. Test class - BankTester --- calls all above behaviors --> Console App

