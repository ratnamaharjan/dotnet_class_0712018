﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace GenericCollExample
{
    class Program
    {
        public delegate void del_PrintCourse(DotNetLesson dotNetLesson);
        static void Main(string[] args)
        {
            //Collections are not type safe 
            ArrayList a = new ArrayList();

            a.Add(100);
            a.Add("test");            

            //List
            List<int> myList = new List<int>();
            myList.Add(1);
            myList.Add(2);

            myList.AddRange(new List<int>{ 4,5,6});

            List<DotNetLesson> listOfDotNetLesson = new List<DotNetLesson>
            {
                new DotNetLesson{ CourseTopic = "ArrayList"},
                new DotNetLesson { CourseTopic = "Collection"}
            };

            myList.IndexOf(0);

            if (listOfDotNetLesson.Contains(new DotNetLesson { CourseTopic = "Generics" }))
            {
                //go to class today
            }
            else
            {
                //escape today's class 
            }


            //SortedList mysortedlist = new SortedList();
            //mysortedlist.Add(3, "test");
            //mysortedlist.Add(1, new DotNetLesson());

            //SortedList<int, string> myGenSortedList = new SortedList<int, string>();
            //myGenSortedList.Add(1, "test");
            //myGenSortedList.Add(0, "test2");


            //SortedList<string, string> myStrSortedList = new SortedList<string, string>();
            //myStrSortedList.Add("Apple", "5000");
            //myStrSortedList.Add("Mango", "50");
            //myStrSortedList.Add("Banana", "779797");

            //var firstItem = myStrSortedList.IndexOfKey("Apple");
            //var myFirstValue = myStrSortedList["Apple"];

            //foreach (KeyValuePair<string, string> item in myStrSortedList)
            //{
                
            //}

            ////Dictionary of Key - Value
            //Dictionary<long, EmployeeAddress> employeeAddressDicColl = new Dictionary<long, EmployeeAddress>();
            //employeeAddressDicColl.Add(775775757, new EmployeeAddress { });
            //employeeAddressDicColl.Add(775775754, new EmployeeAddress { });
            //employeeAddressDicColl.Add(775775755, new EmployeeAddress { });
            //employeeAddressDicColl.Add(775775758, new EmployeeAddress { });
            //employeeAddressDicColl.Add(775775759, new EmployeeAddress { });
            
            //Console.WriteLine(myList.Count + " " + listOfDotNetLesson.Count);

            var param = new DotNetLesson { CourseTopic = "Nepali" };
            //del_PrintCourse printCourse = PrintCourseInfo;
            //printCourse.Invoke(param);
            //printCourse(param, null);
            //Action act = PrintStatus;
            //PrintCourseInfo(param, act);

            Func<string, int, string> myFunc = PrintSomething;
            var result = myFunc("hello", 1);
            Console.WriteLine(result);
            Console.ReadKey();
            
            
            
        }

        public static void PrintStatus()
        {
            Console.WriteLine("printing statussss");
        }

        public static void PrintFinalStatus()
        {
        }

        public static string PrintSomething(string input, int inputInt)
        {
            var s = "Testing in progress";
            Console.WriteLine(s);
            return s;
        }

        public static void PrintCourseInfo(DotNetLesson dotNetLesson, Action callback = null)
        {

            for (int i = 0; i < 5000; i++)
            {
                if (i == 2000)
                {
                    callback.Invoke();
                }
                if (i == 4999)
                    callback.Invoke();
            }
            Console.WriteLine(dotNetLesson.CourseTopic);
        }
    }

    class DotNetLesson
    {
        public string CourseTopic { get; set; }

        public void Test()
        {
            
            Action action = Program.PrintFinalStatus;
            Program.PrintCourseInfo(this, action);
        }
    }

    class EmployeeAddress
    {
    }


}
