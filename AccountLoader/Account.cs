﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountLoader
{
    public class Account
    {
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public double CurrentBalance { get; set; }
    }
}
