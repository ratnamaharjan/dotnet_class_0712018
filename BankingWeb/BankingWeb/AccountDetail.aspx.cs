﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BankingWeb
{
    public partial class AccountDetail : System.Web.UI.Page
    {
        //query database
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //read value from URL                
                var accountNumber = Request.QueryString["id"];
                if (accountNumber == null)
                {
                    Response.Redirect("BankAccount.aspx?msg=Unable to receive account number");
                }
                //Get data from Database and Display on the web form
                PopulateAccountDetail(accountNumber.ToString());
            }
        }

        protected void btnUpdateAccount_Click(object sender, EventArgs e)
        {
            //Update
            UpdateAccount();
        }

        private void PopulateAccountDetail(string accountNumber)
        {
            DataSet ds = new DataSet();            
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var selectQeury = "SELECT * FROM [dbo].[Account] WHERE AccountNumber = @accountNumber";
                SqlCommand cmd = new SqlCommand(selectQeury, connection);
                cmd.Parameters.AddWithValue("@accountNumber", accountNumber);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                
                adapter.Fill(ds);                
            }

            //bind to web controls
            var accountInfo = ds.Tables[0];
            if (accountInfo != null && accountInfo.Rows.Count > 0)
            {
                ddlAccountType.SelectedValue = accountInfo.Rows[0]["AccountType"].ToString();
                txtAccountNumber.Text = accountInfo.Rows[0]["AccountNumber"].ToString();
                txtCurrentBalance.Text = accountInfo.Rows[0]["AvailableBalance"].ToString();
            }
        }

        private void UpdateAccount()
        {
            string error = string.Empty;
            try
            {
                //need update info
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //open connection
                    conn.Open();
                    //update command
                    SqlCommand cmd = new SqlCommand("Update Account set AccountType = @accountType, AvailableBalance = @balance WHERE AccountNumber = @accountNumber", conn);
                    cmd.Parameters.AddWithValue("@accountType", ddlAccountType.SelectedValue);
                    cmd.Parameters.AddWithValue("@balance", txtCurrentBalance.Text);
                    cmd.Parameters.AddWithValue("@accountNumber", txtAccountNumber.Text);
                    //update in database
                    var result = cmd.ExecuteNonQuery();
                    //close db connection
                    conn.Close();
                    //go to bank account list page
                    Response.Redirect("BankAccount.aspx");
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                //go to bank account list page with error message
                Response.Redirect($"BankAccount.aspx?msg={error}");
            } 
        }
    }
}