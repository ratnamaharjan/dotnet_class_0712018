﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4Lab
{
    public class Employee
    {
        public static string carCode; 
        public int EmployeeId { get; set; }
        public string Address { get; set; }
        
        public Employee()
        {
            if (carCode == null)
            {
                carCode = "CARCODE";
            }            
            Console.WriteLine("Default Constructor "+ carCode);
        }

        public Employee(int empId, string address)
        {
            EmployeeId = empId;
            Address = address;
            carCode = "TEST";
            Console.WriteLine("Overload Constructor " + carCode);
        }
    }
}
