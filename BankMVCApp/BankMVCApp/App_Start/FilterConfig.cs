﻿using System;
using System.Web;
using System.Web.Mvc;

namespace BankMVCApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new BankErrorHandler());
        }
    }

    public class BankErrorHandler : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            logError(filterContext.Exception);
            base.OnException(filterContext);
        }

        public void logError(Exception ex)
        {
            //save log to DB 
        }
    }
}
