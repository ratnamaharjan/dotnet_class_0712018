﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class ElectronicCheck: Transaction
    {
        public TypeOfCheck TypeOfCheck { get; set; }
    }

    public enum TypeOfCheck
    {
        PaperCheck,
        ECheck
    }
}
