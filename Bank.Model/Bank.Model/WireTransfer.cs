﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class WireTransfer: Transaction
    {
        public string BeneficiaryFirstName { get; set; }
        public string BeneficiaryLastName { get; set; }
        public string IntermediateBankSwiftCode { get; set; }
        public string BeneficiaryAccountName { get; set; }
    }
}
