﻿using BankDB.Model;
using BankMVCApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace BankMVCApp.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection, CustomerViewModel model)
        {
            bool createCheckingAccount = collection["CheckingAccount"]?.ToString() == "1" ? true : false;
            bool createSavingAccount = collection["SavingAccount"]?.ToString() == "2" ? true : false;
            CreateDbEntry(model, createCheckingAccount, createSavingAccount);
            return RedirectToAction("Index", "Dashboard");
        }

        private void CreateDbEntry(CustomerViewModel model, bool checking, bool saving)
        {
            //create instance of DB context
            BankDBEntities dbContext = new BankDBEntities();
            //Create DB entities
            using (TransactionScope scope = new TransactionScope())
            {
                //customer 
                Customer cust = new Customer
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    CreatedDateTime = DateTime.Now
                };
                dbContext.Customers.Add(cust);
                Address mailing = new Address
                {
                    StreetName = model.MailingAddress.StreetName,
                    City = model.MailingAddress.City,
                    State = model.MailingAddress.State,
                    Zip = model.MailingAddress.Zip,
                    AddressType = "Mailing"                   
                };
                Address billing = new Address
                {
                    StreetName = model.BillingAddress.StreetName,
                    City = model.BillingAddress.City,
                    State = model.BillingAddress.State,
                    Zip = model.BillingAddress.Zip,
                    AddressType = "Billing"
                };
                dbContext.Addresses.Add(mailing);
                dbContext.CustomerAddresses.Add(new CustomerAddress
                                                {
                                                    AddressId = mailing.AddressId,
                                                    CustomerId = cust.CustomerId
                                                });
                //dbContext.Addresses.Add(billing);
                //dbContext.CustomerAddresses.Add(new CustomerAddress
                //{
                //    AddressId = billing.AddressId,
                //    CustomerId = cust.CustomerId
                //});
                bool checkingCreated = false;
                //Save entities
                if (checking)
                {                   
                    //create checking
                    Account act = new Account
                    {
                        AccountTypeId = 1,
                        AccountNumber = GenerateAccountNumber(dbContext, checkingCreated),
                        AvailableBalance = 0,
                        OpenedDateTime = DateTime.Now,
                        CustomerId = cust.CustomerId
                    };
                    dbContext.Accounts.Add(act);
                    checkingCreated = true;
                }
                if (saving)
                {
                    Account actSav = new Account
                    {
                        AccountTypeId = 2,
                        AccountNumber = GenerateAccountNumber(dbContext, checkingCreated),
                        AvailableBalance = 0,
                        OpenedDateTime = DateTime.Now,
                        CustomerId = cust.CustomerId
                    };
                    dbContext.Accounts.Add(actSav);
                }
                dbContext.SaveChanges();
                scope.Complete();
            }
        }

        private string GenerateAccountNumber(BankDBEntities context, bool isCheckingCreated)
        {
            var existingAct = context.Accounts.OrderByDescending(x => x.AccountId).FirstOrDefault();
            if (existingAct != null)
            {
                //add + 1 
                var actNum = existingAct.AccountNumber;
                var newAct = isCheckingCreated ? Convert.ToInt64(actNum) + 2 : Convert.ToInt64(actNum) + 1;
                return newAct.ToString();
            }
            else {
                return "100000088";
            }
        }
    }
}