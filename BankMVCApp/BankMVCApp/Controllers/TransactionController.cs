﻿using BankDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace BankMVCApp.Controllers
{
    public class TransactionController : Controller
    {
        // GET: Transaction
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Create(int id)
        {
            ViewBag.AccountId = id;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create(TransactionDetail transaction)
        {
            //HTTP Client
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:57042/api/");
            HttpResponseMessage response = await client.PostAsJsonAsync("Transaction", transaction);
            
            //Make Web API Call
            //Check response
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                //failure
            }
            //base on the response we are going to redirect
            return RedirectToAction("Index","Dashboard");
        }
    }
}