﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BankAccount.aspx.cs" Inherits="BankingWeb.BankAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table>
            <thead>
                <tr>
                    <td colspan="3" style="background-color: #808080; color: white;">Add Account</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Account Type: </td>
                    <td>
                        <asp:DropDownList ID="ddlAccountType" runat="server">
                            <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Checking" Value="Checking"></asp:ListItem>
                            <asp:ListItem Text="Saving" Value="Saving"></asp:ListItem>
                            <asp:ListItem Text="Business Checking" Value="BusinessChecking"></asp:ListItem>
                            <asp:ListItem Text="Mortgage" Value="Mortgage"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td>
                        <asp:RequiredFieldValidator ID="ddlAccountTypeValidator"
                            runat="server" ErrorMessage="*" ControlToValidate="ddlAccountType"
                            Display="Dynamic" ForeColor="#FF3300"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Account Number: </td>
                    <td>
                        <asp:TextBox ID="txtAccountNumber" runat="server"></asp:TextBox></td>
                    <td>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAccountNumber"
                            Display="Dynamic" ErrorMessage="Account Number Cannot be empty" Font-Bold="True" ForeColor="#FF3300"></asp:RequiredFieldValidator>

                    </td>
                </tr>
                <tr>
                    <td>Current Balance:</td>
                    <td>
                        <asp:TextBox ID="txtCurrentBalance" runat="server"></asp:TextBox></td>
                    <td>

                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic" ControlToValidate="txtCurrentBalance"
                            ErrorMessage="Invalid Amount" ForeColor="Red" ValidationExpression="\d"></asp:RegularExpressionValidator>--%>

                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label runat="server" ID="lblErrorMessage" Text="No Error" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;">
                        <asp:Button ID="btnAddAccount" runat="server" Text="Add Account" OnClick="btnAddAccount_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
        <hr />
        <div style="width:300px; height:300px; overflow-x:scroll; overflow-y:scroll;">
            <h2>Accounts</h2>
            <asp:GridView ID="gvAccounts" runat="server" ShowHeader="true" AllowPaging="true" PageSize="5"
                AllowSorting="true" AutoGenerateColumns="false" AlternatingRowStyle-BackColor="ButtonShadow"
                OnSelectedIndexChanged="gvAccounts_SelectedIndexChanged" AutoGenerateSelectButton="true">
                <Columns>
                    <asp:BoundField DataField="AccountType" HeaderText="Type" />
                    <asp:BoundField DataField="AccountNumber" HeaderText="Act Number"/>
                    <asp:BoundField DataField="CurrentBalance" HeaderText="Balance" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="AccountOpenDate" HeaderText="Date Opened" DataFormatString="{0:mm/dd/yyyy}" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
