﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            using (StreamReader reader = new StreamReader(@"C:\Projects\InputData\Account.txt"))
            {
                string lineOfData;
                List<Account> listOfAccount = new List<Account>();
                while ((lineOfData = reader.ReadLine()) != null)
                {
                    Account act = new Account();
                    //AccountType:Saving , AccountNumber:22355679 ,  CurrentBalance:59909.00
                    var splitData = lineOfData.Split(',');
                    if (splitData.Length > 0)
                    {
                        foreach(string str in splitData) //AccountType : Saving
                        {
                            var resultFromFinalSplit = str.Split(':');
                            if (resultFromFinalSplit[0] == "AccountType")
                            {
                                act.AccountType = resultFromFinalSplit[1];
                            }
                            else if (resultFromFinalSplit[0] == "AccountNumber")
                            {
                                act.AccountNumber = resultFromFinalSplit[1];
                            }
                            else if (resultFromFinalSplit[0] == "CurrentBalance")
                            {
                                act.CurrentBalance = double.Parse(resultFromFinalSplit[1]);
                            }
                        }
                        listOfAccount.Add(act);
                    }
                }
                foreach(Account act in listOfAccount)
                {
                    Console.WriteLine("Account Type: {0}  Account Number: {1}  Current Balance: {2}", act.AccountType, act.AccountNumber, act.CurrentBalance.ToString("c"));
                }
            }
            Console.ReadKey();
        }
    }
}
