﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace CollectionExample
{
    class Program
    {
        static void Main(string[] args)
        {          
            //ArrayList example

            string[] myStringArray = new string[90];
            myStringArray[0] = "string";

            ArrayList myArrayList = new ArrayList();
            myArrayList.Add(500);
            Console.WriteLine("Item count in ArrayList: {0}", myArrayList.Count);

            myArrayList.Add("test");
            Employee emp = new Employee { Salary = 5000 };
            myArrayList.Add(emp);
            Console.WriteLine("Item count in ArrayList: {0}", myArrayList.Count);

            myArrayList.Add("jksldfld");
            myArrayList.Add("tesllse");

            //Insert item 1000 to 0 th position
            myArrayList.Insert(0, 1000);
            ICollection c = new ArrayList { "666", "testuuuuu" };
            myArrayList.InsertRange(2, c);

            //Contains return boolean ( true if exists, false if it does not)
            var doesExists = myArrayList.Contains("Ratna");
            Console.WriteLine(myArrayList.Count);


            //removes everything from the arraylist
            myArrayList.Clear();
            Console.WriteLine(myArrayList.Count);


            //SortedList Example
            SortedList sortedLst = new SortedList();
            sortedLst.Add("test", "kjsldfkdlf");
            sortedLst.Add("rest", "jlsdfjkld");
            sortedLst.Add("dallas", "city");

            //sortedLst.RemoveAt(1);

            for (int i = 0; i < sortedLst.Count; i++)
            {
                //Console.WriteLine("Key " + sortedLst.GetKey(i) + " value " + sortedLst.GetByIndex(i));
            }

            //Using Dictionary entry you can access the items
            foreach (DictionaryEntry item in sortedLst)
            {
                //Console.WriteLine((string)item.Key);
                //Console.WriteLine((string)item.Value);
            }

            //Stack 
            Employee emp1 = new Employee { EmployeeName = "Jim" };
            Employee emp2 = new Employee { EmployeeName = "Tim" };
            Employee emp3 = new Employee { EmployeeName = "Sim" };
            Stack stackOfEmployee = new Stack();
            stackOfEmployee.Push(emp1);
            stackOfEmployee.Push(emp2);
            stackOfEmployee.Push(emp3);

            var peekItem = stackOfEmployee.Peek();


            Console.WriteLine("Count of emp in stack {0}", stackOfEmployee.Count);
            Console.WriteLine("Peek Item {0}", ((Employee)peekItem).EmployeeName);

            var popItem = stackOfEmployee.Pop();
            Console.WriteLine("Count of emp in stack {0}", stackOfEmployee.Count);
            var anotherPeek = stackOfEmployee.Peek();
            Console.WriteLine("Peek Item after pop {0}", ((Employee)anotherPeek).EmployeeName);

            if (stackOfEmployee.Contains(emp1))
            {
                Console.WriteLine("Jim is there");
            }
            Console.WriteLine(stackOfEmployee.Count);
            stackOfEmployee.Clear();
            Console.WriteLine(stackOfEmployee.Count);

            //Queue example
            Queue empQueue = new Queue();
            empQueue.Enqueue(emp1);
            empQueue.Enqueue(emp2);
            empQueue.Enqueue(emp3);


            foreach (var item in empQueue)
            {
                Console.WriteLine(((Employee)item).EmployeeName);
            }
            Console.WriteLine("Count before dequeue {0}", empQueue.Count);
            if (empQueue.Count > 0)
            {
                var dequeuItem = empQueue.Dequeue();
            }
            Console.WriteLine("Count after dequeue {0}", empQueue.Count);
            Console.WriteLine("HashCode");
            Console.WriteLine(emp1.GetHashCode());

            //Hashtable 

            Hashtable hashTable = new Hashtable();
            hashTable.Add(1, emp1);
            hashTable.Add("kksiidkkfuukdkdiuuidiid", emp2);
            hashTable.Add("test", emp3);
            
            foreach(DictionaryEntry item in hashTable)
            {
                Console.WriteLine($"Key: {item.Key} Hash Code: {item.Key.GetHashCode()}");
            }

            Console.WriteLine("Hash Table Item count {0}", hashTable.Count);
            Console.ReadKey();
        }
    }

    class Employee
    {
        public string EmployeeName { get; set; }
        public double Salary { get; set; }
    }
}
