﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Program.Calculate();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception during calculation or user input. ExceptionInfo {0}", ex.Message);
            }
            Console.ReadKey();
        }

        static void Calculate()
        {
            while (true)
            {
                Console.WriteLine("Select options: \n1. Add \n2. Substract \n3. Multiply \n4. Divide \n5. Exit");
                int optionSelected = int.Parse(Console.ReadLine());
                if (optionSelected == 5)
                {
                    break;
                }
                if (optionSelected <= 0 || optionSelected > 5)
                {
                    Console.WriteLine("Invalid Selection. Try again!");
                    continue;
                }
                Console.WriteLine("Enter two numbers:\nFirst :");
                double firstNumber = double.Parse(Console.ReadLine());
                Console.WriteLine("Second :");
                double secondNumber = double.Parse(Console.ReadLine());                
                string selectedOptionText = "";
                double result = 0.0;
                bool isInvalidOperaton = false;
                switch (optionSelected)
                {
                    case 1:
                        //add 
                        selectedOptionText = "Addition";
                        result = firstNumber + secondNumber;
                        break;
                    case 2:
                        selectedOptionText = "Subtraction";
                        result = firstNumber - secondNumber;
                        break;
                    case 3:
                        selectedOptionText = "Multiplication";
                        result = firstNumber * secondNumber;
                        break;
                    case 4:
                        selectedOptionText = "Division";
                        if (secondNumber == 0)
                        {
                            Console.WriteLine("Division by 0 encounterd. Invalid!");
                            isInvalidOperaton = true;
                            break;
                        }
                        result = firstNumber / secondNumber;
                        break;
                    case 5:
                        Console.WriteLine("Do you want to exit? Hit enter for exit");
                        Console.ReadKey();
                        break;
                    default:
                        Console.WriteLine("You have selected invalid option!");
                        break;
                }
                
                if (!isInvalidOperaton)
                {
                    Console.WriteLine("Operation: " + selectedOptionText + "; Inputs : "
                                        + firstNumber.ToString() + " "
                                        + secondNumber.ToString() + "; "
                                        + "Result " + result);
                }
            }
            
        }
    }
}
