﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class Bank
    {
        public string BankName { get; set; }
        public List<Customer> Customers { get; set; }

        public Bank()
        {
            Customers = new List<Customer>();
        }

        public void PrintBankName()
        {
            Console.WriteLine("** start PrintBankName**");
            Console.WriteLine("Bank Name: {0}", BankName);
            Console.WriteLine("** Finished PrintBankName**");
        }

        public void PrintBankDetails()
        {
            Console.WriteLine("** start PrintBankDetails**");
            Console.WriteLine("Bank Name: {0}", BankName);
            foreach (Customer cust in Customers)
            {
                //do this
                Console.WriteLine("Customer Name: {0},  Address {1}", cust.CustomerName, cust.Address);
            }
            Console.WriteLine("** finished PrintBankDetails**");
        }
    }
}
