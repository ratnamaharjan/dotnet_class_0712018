﻿using SampleArchitectureProject.DataAccess;
using SampleArchitectureProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleArchitectureProject.BusinessLogic
{
    public class EmployeeService
    {
        public IEnumerable<Employee> GetEmployeeData()
        {
            var employeeDataService = new EmployeeData();
            var result = employeeDataService.GetEmployeeData();
            // some logic

            return new List<Employee>();
        }
    }
}
