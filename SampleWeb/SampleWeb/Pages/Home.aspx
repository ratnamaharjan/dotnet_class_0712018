﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="SampleWeb.Pages.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sample Web Site</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Create Account</h1>
            <table>
                <tr>
                    <td>Account Type</td>
                    <td><asp:DropDownList runat="server" ID="ddlAccountType"><asp:ListItem Text="Checking" Value="Checking"></asp:ListItem> 
                                             </asp:DropDownList></td>
                    <td></td>
                    <td><asp:Button ID="btnSave" runat="server" Text="Save Account" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
