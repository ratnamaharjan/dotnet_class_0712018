﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class Transaction
    {
        public double Amount { get; set; }
        public double Fee { get; set; }
        public string ReceiverFirstname { get; set; }
        public string ReceiverLastName { get; set; }

        //Create transaction
        public void CreateTransaction()
        {
        }

        //deduct account balance
        public void DeductAccountBalance()
        {
        }
    }
}
