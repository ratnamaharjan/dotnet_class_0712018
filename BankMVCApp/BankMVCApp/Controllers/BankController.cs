﻿using BankDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BankMVCApp.Controllers
{
    [Authorize]
    [BankErrorHandler]
    public class BankController : Controller
    {
        //private List<Account> listOfAccounts = new List<Account>();
        public BankController()
        {            
            //listOfAccounts.Add(new Account { AccountNumber = "456565", AccountType = "Checking", Balance = 5000000 });
            //listOfAccounts.Add(new Account { AccountNumber = "998898", AccountType = "Saving", Balance = 6889888 });
            //listOfAccounts.Add(new Account { AccountNumber = "99099", AccountType = "Checking", Balance = 5000000 });
            //listOfAccounts.Add(new Account { AccountNumber = "778777", AccountType = "Saving", Balance = 6889888 });
            //listOfAccounts.Add(new Account { AccountNumber = "77877", AccountType = "Saving", Balance = 5000000 });
            //listOfAccounts.Add(new Account { AccountNumber = "88987", AccountType = "Checking", Balance = 6889888 });
            //listOfAccounts.Add(new Account { AccountNumber = "77876", AccountType = "Saving", Balance = 5000000 });
        }
        // GET: Bank
        [BankErrorHandler]
        public ActionResult Index()
        {
            BankDBEntities bankDBEntities = new BankDBEntities();
            if (TempData["CreateMessage"] != null)
            {
                ViewBag.Message = TempData["CreateMessage"].ToString();
            }
            return View(bankDBEntities.Accounts);
        }

        // GET: Bank/Details/5
        public ActionResult Details(string id)
        {            
            return View();
        }

        // GET: Bank/Create
        public ActionResult Create()
        {
            BankDB.Model.BankDBEntities bankDBEntities = new BankDB.Model.BankDBEntities();
            var accountTypes = new List<SelectListItem>();
            foreach (var item in bankDBEntities.AccountTypes)
            {
                var drpItem = new SelectListItem { Text = item.AccountType1, Value = item.AccountTypeId.ToString() };
                accountTypes.Add(drpItem);
            }            
            ViewBag.AccountItemList = accountTypes;
            return View();
        }

        // POST: Bank/Create
        [HttpPost]
        public ActionResult Create(Account account)
        {
            BankDBEntities bankDBEntities = new BankDB.Model.BankDBEntities();
            try
            {
                
                bankDBEntities.Accounts.Add(account);
                bankDBEntities.SaveChanges();
                TempData["CreateMessage"] = $"Account {account.AccountNumber} was successfully saved to database!";
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                var accountTypes = new List<SelectListItem>();
                foreach (var item in bankDBEntities.AccountTypes)
                {
                    var drpItem = new SelectListItem { Text = item.AccountType1, Value = item.AccountTypeId.ToString() };
                    accountTypes.Add(drpItem);
                }
                ViewBag.AccountItemList = accountTypes;
                return View();
            }
        }

        [Authorize(Roles = "Admin")]
        // GET: Bank/Edit/5
        public ActionResult Edit(string id)
        {            
            return View();
        }

        // POST: Bank/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Bank/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Bank/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
