﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumberChecker
{
    class Program
    {
        public static void Main(string[] args)
        {
            var maxNumber = 100;
            for (int i = 2; i < maxNumber; i++)
            {
                if (i == 2)
                {
                    Console.Write(i + " ");
                    continue;
                }

                bool isPrime = true;
                for (int j = 2; j <= Math.Sqrt(i); j++)
                {
                    if (i % j == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime)
                    Console.Write(i + " ");
            }

            Console.ReadKey();
        }
    }
}
