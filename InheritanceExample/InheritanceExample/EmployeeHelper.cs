﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExample
{
    partial class EmployeeHelper
    {
        public static double CalculateEmployeeSalary(double monthlySalary)
        {
            return monthlySalary * 12;
        }

        public void PrintSchedule()
        {
            Console.WriteLine("Printing schedule");
        }
    }

    sealed class CustomerHelper
    {
        public static void PrintCustomer()
        {
        }
    }

    partial class EmployeeHelper
    {
        public void IamFromSecondPartialClass()
        {
        }
    }
}
