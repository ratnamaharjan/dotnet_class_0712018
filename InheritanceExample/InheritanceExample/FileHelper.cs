﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace InheritanceExample
{
    public class FileHelper
    {
        //read and write 
        public void ReadFile(string filePath)
        {
            try
            {
                //read file
                var fileData = File.ReadAllLines(filePath);
                foreach (string s in fileData)
                {
                    Console.WriteLine(s);
                }
            }
            catch (Exception ex)
            {
                //
            }
            
        }

        public void WriteToFile(List<string> inputData, string outPutFilePath)
        {
            try
            {
                if (!File.Exists(outPutFilePath))
                {
                    File.Create(outPutFilePath);
                }
                //write to file
                File.WriteAllLines(outPutFilePath, inputData);
            }
            catch (Exception ex)
            {
                //
            }
        }
    }
}
