﻿using BankDB.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace BankAPI.Controllers
{
    public class TransactionController : ApiController
    {
        // GET: Transaction
        [System.Web.Http.HttpGet]
        public IEnumerable<TransactionDetail> Get([FromUri] int id)
        {
            return null;
        }

        [HttpPost]
        public TransactionDetail Post([FromBody]TransactionDetail transactionDetail)
        {
            var dbContext = new BankDBEntities();
            dbContext.TransactionDetails.Add(transactionDetail);
            
            var retult = dbContext.SaveChanges();
            return transactionDetail;
        }
    }
}