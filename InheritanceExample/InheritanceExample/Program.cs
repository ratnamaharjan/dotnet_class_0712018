﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee e = new Employee();
            e.FirstName = "John";
            e.LastName = "lastname";

            //IEmployeeService employeeService = new EmployeeService();
            //employeeService.AddEmployee(e);

            Console.WriteLine(EmployeeHelper.CalculateEmployeeSalary(5000));

            EmployeeHelper eHelper = new EmployeeHelper();
            eHelper.PrintSchedule();
            eHelper.IamFromSecondPartialClass();
                        

            Console.ReadKey();
           
            
        }
    }
}
