﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AccountDetail.aspx.cs" Inherits="BankingWeb.AccountDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
            <thead>
                <tr>
                    <td colspan="3" style="background-color: #808080; color: white;">Add Account</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Account Type: </td>
                    <td>
                        <asp:DropDownList ID="ddlAccountType" runat="server">
                            <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Checking" Value="Checking"></asp:ListItem>
                            <asp:ListItem Text="Saving" Value="Saving"></asp:ListItem>
                            <asp:ListItem Text="Business Checking" Value="BusinessChecking"></asp:ListItem>
                            <asp:ListItem Text="Mortgage" Value="Mortgage"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td>
                        <asp:RequiredFieldValidator ID="ddlAccountTypeValidator"
                            runat="server" ErrorMessage="*" ControlToValidate="ddlAccountType"
                            Display="Dynamic" ForeColor="#FF3300"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Account Number: </td>
                    <td>
                        <asp:TextBox ID="txtAccountNumber" runat="server" Enabled="false"></asp:TextBox></td>
                    <td>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAccountNumber"
                            Display="Dynamic" ErrorMessage="Account Number Cannot be empty" Font-Bold="True" ForeColor="#FF3300"></asp:RequiredFieldValidator>

                    </td>
                </tr>
                <tr>
                    <td>Current Balance:</td>
                    <td>
                        <asp:TextBox ID="txtCurrentBalance" runat="server"></asp:TextBox></td>
                    <td>                        
                    </td>
                </tr>                      
                
                <tr>
                    <td colspan="3" style="text-align: right;">
                        <asp:Button ID="btnUpdateAccount" runat="server" Text="UpdateAccount" OnClick="btnUpdateAccount_Click"/>
                    </td>
                </tr>
            </tbody>
        </table>
</asp:Content>
