﻿using BankDB.Model;
using BankMVCApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BankMVCApp.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            var data = PopulateDashboardModel();

            return View(data);
        }

        public ActionResult CustomerDetails(int id)
        {
            //db context 
            var bankDBEntities = new BankDBEntities();
            //initialize view model
            var custDetails = new CustomerDetailViewModel();
            //retrive customers by id
            var custDetailEntities = bankDBEntities.Customers.FirstOrDefault(x => x.CustomerId == id);
            //populate view model
            custDetails.FirstName = custDetailEntities.FirstName;
            custDetails.LastName = custDetailEntities.LastName;
            custDetails.CustomerSince = custDetailEntities.CreatedDateTime;
            custDetails.Accounts = custDetailEntities.Accounts.ToList();
            custDetails.Addresses = new List<Address>();
            foreach (var item in custDetailEntities.CustomerAddresses)
            {
                custDetails.Addresses.Add(item.Address);
            }
            //return partial view with data
            return PartialView("_CustomerDetail", custDetails);
        }

        private DashboardModel PopulateDashboardModel()
        {
            var dashboardModel = new DashboardModel();
            var bankDBEntities = new BankDBEntities();
            var customerEntities = bankDBEntities.Customers;
            dashboardModel.Customers = customerEntities.ToList();
            return dashboardModel;
        }
    }
}